import React from "react";
import Todo from './Todo'

const Todos = () => {
    return (
        <div className ="list-task">
        	<div className="title-list">
        		<h2>List of Task</h2>
        	</div>           
            <div>
                <Todo />
                <Todo />
                <Todo />
                <Todo />
            </div>
        </div>

    )
}

export default Todos
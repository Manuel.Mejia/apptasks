import React from "react";

const Todo = () => {
    return (
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
               <button type="button" className="btn btn-success btn-sm">Update</button>
                <button type="button" className="btn btn-danger btn-sm">Delete</button>
            </div>
        </div>

            )
}

export default Todo
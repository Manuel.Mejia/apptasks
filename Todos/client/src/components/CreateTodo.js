import React from 'react'
import { } from 'react-router-dom'

const CreateTodo = () => {

    

    const createTask = (e)=>{
        const title = document.getElementById("title").value
        const  description = document.getElementById("description").value
        alert(`title : ${title}
                description: ${description}`)
    }

    return (
        <div className="div-create row">
            <form>
                <div class="form-group">
                    <label for="formGroupExampleInput">Task Name:</label>
                    <input type="text" class="form-control" id="title" placeholder=""/>
                </div>
                <div class="form-group task-des">
                    <label for="formGroupExampleInput2">Task Description:</label>
                    <input type="text" class="form-control task-des" id="description" placeholder=""/>
                </div>
            </form>
            <button type="submit" class="btn btn-primary btn-create" id="btnCreate"onClick = {createTask}>Create</button>
        </div>
    )
}

export default CreateTodo
import React from "react";
import { Link } from 'react-router-dom'

const Nav = () => {
    return (
        <nav>
            <ul class="nav justify-content-center ">
                <li class="nav-item">
                    <Link to='/Todos' className='nav-link active'>Tasks</Link>
                </li>
                <li class="nav-item">
                    <Link to='/Create' className='nav-link active'>Create Task</Link>
                </li>
            </ul>
        </nav>
    )
}

export default Nav;
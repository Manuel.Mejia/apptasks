
import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Nav from './components/Nav'
import Todos from './components/Todos'
import CreateTodo from './components/CreateTodo';
import './App.css';


function App() {
  return (
    <Router>
      <Nav />
      <Route path='/Todos' component={Todos} />
      <Route path='/Create' component={CreateTodo} />
    </Router>
  );
}

export default App;

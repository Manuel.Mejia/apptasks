const mongoose = require('mongoose')

URI = 'mongodb+srv://noah:*****!@cluster0.s96le.mongodb.net/todos?retryWrites=true&w=majority'
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

module.exports = mongoose.connect(URI, options)
    .then(db => console.log(`Connect to ${db}`))
    .catch(err => console.log(err))


const express = require('express')
const dbs = require('../controllers/todo.controller')
const mongoose = require('mongoose')
const router = express.Router()

router.get('/', async (req, res) => {
    const todos = await dbs.show()
    res.json(todos)

})

router.get('/:id', async (req, res) => {
    const id = req.params.id

    const todo = await dbs.showOne(id)
    
    if(todo){
        res.json({
            data: todo
        })
    }else{
        res.json({
            message: false
        })
    }
})

router.post('/', async (req, res) => {
    const title = req.body.title
    const description = req.body.description
    const date = Date.now()

    const todo = await dbs.insert(title, description, date)
    res.json({
        message: "Todo save"
    })
})

router.put('/:id', async (req, res) => {
    const id = req.params.id
    const data = req.body

    const todo = await dbs.update(id, data)

    if (todo) {
        res.json({
            message: "Update Todo"
        })
    } else {
        res.json({
            message: "Update not Todo"
        })
    }
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id

    const todo = await dbs.deleted(id)

    if (todo) {
        res.json({
            message: "Deleted todo"
        })
    } else {
        res.json({
            message: "Deleted not todo"
        })
    }
})

module.exports = router;
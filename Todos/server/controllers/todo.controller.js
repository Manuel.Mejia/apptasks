const Todo = require('../models/todo.model')

const dbs = {}

const show = async () => {
    try {
        const todos = await Todo.find({})
        return todos
    } catch (err) {
        console.log(err)
    }
}

const showOne = async (id) =>{
    try{    
        const todo = await Todo.findOne({_id:id})
        if(todo){
            return todo
        }else{
            return false
        }
    }catch(err){

    }
}

const insert = async (title, description, date) => {
    try {

        const newTodo = new Todo({
            "title": `${title}`,
            "description": `${description}`,
            "createDate": `${date}`
        })


        const todo = await newTodo.save(err => {
            if (err) {
                return handelError(err)
            }else{
                return false
            }
        })
        if (todo != null) {
            return todo;
        }
    } catch (err) {
        console.log(err)
    }
}

const update = async (id, data) => {
    try {

        const todo = await Todo.findByIdAndUpdate(id, { title: data.title, description: data.description })
        if (todo) {
            return todo
        }else{
            return false
        }
    } catch (err) {
        console.log(err)
    }
}

const deleted = async (id) => {
    try {
        const todo = await Todo.findOneAndRemove(id)

        if (todo) {
            return todo
        }else{
            return false
        }

    } catch (err) {
        console.log(err)
    }
}

dbs.show = show
dbs.showOne = showOne
dbs.insert = insert
dbs.update = update
dbs.deleted = deleted


module.exports = dbs;
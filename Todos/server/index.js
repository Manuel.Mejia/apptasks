const express = require('express')
const path = require('path')
const cors = require('cors')
const bodyParser = require('body-parser')
require('./config/db')

const app = express()

//middleware
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

//set
app.set('port', process.env.PORT || 9000)

//routes
app.use('/api', require('./routes/todo.route'))
//static 

//start
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
})


